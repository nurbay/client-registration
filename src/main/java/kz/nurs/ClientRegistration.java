package kz.nurs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientRegistration {
    public static void main(String[] args) {
        SpringApplication.run(ClientRegistration.class, args);
    }
}

