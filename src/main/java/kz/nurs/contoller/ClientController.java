package kz.nurs.contoller;

import kz.nurs.entity.Address;
import kz.nurs.entity.Client;
import kz.nurs.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(path = "/client")
public class ClientController {

    @Autowired
    private ClientRepository clientRepository;

    @PostMapping(path = "/add")
    public @ResponseBody
    String addNewClient(@RequestParam String name,
                        @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date foundation_date,
                        @RequestParam String image_url,
                        @RequestParam String addressLine,
                        @RequestParam String countryCode,
                        @RequestParam String cityName) {
        Client c = new Client();
        c.setName(name);
        c.setFoundation_date(foundation_date);
        c.setImage_url(image_url);

        Address a1 = new Address();
        a1.setAddressLine(addressLine);
        a1.setCountryCode(countryCode);
        a1.setCityName(cityName);

        List<Address> addresses = new ArrayList<Address>();
        addresses.add(a1);

        c.setAddress(addresses);
        clientRepository.save(c);
        return "Saved new client: " + c.getName() + " " + c.getFoundation_date();
    }

    @GetMapping(path = "/find")
    public @ResponseBody
    String findClient(@RequestParam("name") String name,
                      @RequestParam(value = "foundation_date_gte", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date_gte,
                      @RequestParam(value = "foundation_date_lte", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date_lte,
                      @RequestParam(required = false) String addressLine,
                      @RequestParam(required = false) String countryCode,
                      @RequestParam(required = false) String cityName) {
        List<Client> clients = clientRepository.findClient(name, date_gte, date_lte, addressLine, countryCode, cityName);

        String msg = "";
        if (clients.isEmpty()) msg = "No client found";
        else msg = "Found client: " + clients.get(0).printClient() +
                "with address: " + clients.get(0).printAddresses();

        return msg;
    }

}

