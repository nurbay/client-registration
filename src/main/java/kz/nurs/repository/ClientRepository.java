package kz.nurs.repository;

import kz.nurs.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface ClientRepository extends JpaRepository<Client, Integer> {

    @Query("SELECT c " +
            "FROM Client c JOIN c.address d " +
            "WHERE LOWER(c.name) = LOWER(:name) " +
            "AND (:foundation_date_gte is NULL OR c.foundation_date > :foundation_date_gte) " +
            "AND (:foundation_date_lte is NULL OR c.foundation_date < :foundation_date_lte) " +
            "AND (d.addressLine=:addressLine OR :addressLine is NULL) " +
            "AND (d.countryCode=:countryCode OR :countryCode is NULL) " +
            "AND (d.cityName=:cityName OR :cityName is NULL)")
    public List<Client> findClient(@Param("name") String name,
                                   @Param("foundation_date_gte") Date date_gte,
                                   @Param("foundation_date_lte") Date date_lte,
                                   @Param("addressLine") String addressLine,
                                   @Param("countryCode") String countryCode,
                                   @Param("cityName") String cityName);

}