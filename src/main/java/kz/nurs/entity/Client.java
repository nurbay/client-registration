package kz.nurs.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Entity
public class Client {

    public Client() {}

    public Client(Integer id, String name, Date f_date, String url, List<Address> address) {
        this.id = id;
        this.name = name;
        this.foundation_date = f_date;
        this.image_url = url;
        this.address = address;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private Date foundation_date;
    private String image_url;

    public List<Address> getAddress() {
        return address;
    }

    public String printAddresses() {

        StringBuilder add = new StringBuilder();
        if (getAddress() == null) add = new StringBuilder();
        else {
            Iterator<Address> it = getAddress().iterator();
            while (it.hasNext()) {
                add.append(it.next().getAddressLine()).append(", ");
            }
        }
        return add.toString();
    }

    public String printClient() {
        return getName() + " " + getFoundation_date() + " " + getImage_url() + "; ";
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }

    @OneToMany(cascade = {CascadeType.ALL})
    private List<Address> address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getFoundation_date() {
        return foundation_date;
    }

    public void setFoundation_date(Date foundation_date) {
        this.foundation_date = foundation_date;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
